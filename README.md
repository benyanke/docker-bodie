# Hello World Docker

A hello world example of a docker build.

## Description
This image contains a simple command line app packaged in a docker container.

## Container Images

The following images are build by CI:

On tags for releases:
 - `registry.gitlab.com/benyanke/docker-bodie:latest`
 - `registry.gitlab.com/benyanke/docker-bodie:{tag}`

On every push to branches:
 - `registry.gitlab.com/benyanke/docker-bodie:branch_{branchname}`
 - `registry.gitlab.com/benyanke/docker-bodie:commit_{commit_hash}`

Note that the `branch_*` and `commit_*` tags are cleaned up regularly.

## Release Process

Test the `commit_*` image after pushing the commit, and do local testing. If it works, make a git tag
matching the pattern `vX.X.X` where `X` is an integer.
