FROM php

WORKDIR /app

COPY bodie.php /app/

RUN php bodie.php

CMD ["/usr/local/bin/php", "/app/bodie.php"]
